#include "GaudiKernel/DeclareFactoryEntries.h"

#include "GepOutputReader/CaloCellsReader.h"
DECLARE_ALGORITHM_FACTORY( CaloCellsReader )

#include "GepOutputReader/WriteOutputHist.h"
DECLARE_ALGORITHM_FACTORY( WriteOutputHist )

#include "GepOutputReader/WriteOutputTree.h"
DECLARE_ALGORITHM_FACTORY( WriteOutputTree )

DECLARE_FACTORY_ENTRIES( GepOutputReader ) 
{
  DECLARE_ALGORITHM( CaloCellsReader );
  DECLARE_ALGORITHM( WriteOutputTree );
  DECLARE_ALGORITHM( WriteOutputHist );
}
