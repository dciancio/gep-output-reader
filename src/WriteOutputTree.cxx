#include "GepOutputReader/WriteOutputTree.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "JetEDM/PseudoJetVector.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"
#include "xAODTau/TauJet.h"
#include "GepOutputReader/TruthTau.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODMissingET/MissingETContainer.h"

using xAOD::Jet;
using jet::PseudoJetVector;

#include <string>

WriteOutputTree::WriteOutputTree( const std::string& name, ISvcLocator* pSvcLocator ) :
  AthAnalysisAlgorithm( name, pSvcLocator )
{
  //declareProperty( "CaloNoiseTool",m_noiseTool,"Tool Handle for noise tool");
  declareProperty( "ClustersList", ClustList);
  declareProperty( "JetsList", JetList);
  declareProperty( "GetEventInfo", m_getEventInfo);
  declareProperty( "GetCellsInfo", m_getCellsInfo);
  declareProperty("GetJetConstituentsInfo", m_getJetConstituentsInfo);
  declareProperty("GetJetSeedsInfo", m_getJetSeedsInfo);
}


WriteOutputTree::~WriteOutputTree() {}


StatusCode WriteOutputTree::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  m_tree = new TTree("ntuple","ntuple");


  //convert input strings to TStrings
  for(unsigned int i=0; i<ClustList.size(); i++)m_cl_list.push_back(ClustList[i]);
  for(unsigned int j=0; j<JetList.size(); j++)m_jet_list.push_back(JetList[j]);


  // clusters
  for(unsigned int j=0; j<m_cl_list.size(); j++){
    ATH_MSG_INFO(m_cl_list[j]);
    m_cl_et[m_cl_list[j]] = {0.};
    m_cl_eta[m_cl_list[j]] = {0.};
    m_cl_phi[m_cl_list[j]] = {0.};
    m_cl_EngFracEM[m_cl_list[j]] = {0.};
    m_cl_m[m_cl_list[j]] = {0.};

    m_tree->Branch(m_cl_list[j]+"_et", &m_cl_et[m_cl_list[j]]);
    m_tree->Branch(m_cl_list[j]+"_eta", &m_cl_eta[m_cl_list[j]]);
    m_tree->Branch(m_cl_list[j]+"_phi", &m_cl_phi[m_cl_list[j]]);
    m_tree->Branch(m_cl_list[j]+"_EngFracEm", &m_cl_EngFracEM[m_cl_list[j]]);
    m_tree->Branch(m_cl_list[j]+"_m",   &m_cl_m[m_cl_list[j]]);

  }

  // jets
  for(unsigned int j=0; j<m_jet_list.size(); j++){
    ATH_MSG_INFO(m_jet_list[j]);
    m_jet_pt[m_jet_list[j]] = {0.};
    m_jet_eta[m_jet_list[j]] = {0.};
    m_jet_phi[m_jet_list[j]] = {0.};
    m_jet_m[m_jet_list[j]] = {0.};
    m_jet_nConstituents[m_jet_list[j]] = {0};

    m_tree->Branch(m_jet_list[j]+"_pt", &m_jet_pt[m_jet_list[j]]);
    m_tree->Branch(m_jet_list[j]+"_eta", &m_jet_eta[m_jet_list[j]]);
    m_tree->Branch(m_jet_list[j]+"_phi", &m_jet_phi[m_jet_list[j]]);
    m_tree->Branch(m_jet_list[j]+"_m",   &m_jet_m[m_jet_list[j]]);
    m_tree->Branch(m_jet_list[j]+"_nConstituents",   &m_jet_nConstituents[m_jet_list[j]]);

    if (m_getJetConstituentsInfo)
    {
      m_jetConst_et[m_jet_list[j]] = {{0.}};
      m_jetConst_eta[m_jet_list[j]] = {{0.}};
      m_jetConst_phi[m_jet_list[j]] = {{0.}};

      m_tree->Branch(m_jet_list[j] + "_constituentPt", &m_jetConst_et[m_jet_list[j]]);
      m_tree->Branch(m_jet_list[j] + "_constituentEta", &m_jetConst_eta[m_jet_list[j]]);
      m_tree->Branch(m_jet_list[j] + "_constituentPhi", &m_jetConst_phi[m_jet_list[j]]);
    }
    if (m_jet_list[j].Contains("Cone"))
    {
      if (m_getJetSeedsInfo)
      {
        m_jetSeed_et[m_jet_list[j]] = {{0.}};
        m_jetSeed_eta[m_jet_list[j]] = {{0.}};
        m_jetSeed_phi[m_jet_list[j]] = {{0.}};

        m_tree->Branch(m_jet_list[j] + "_seedPt", &m_jetSeed_et[m_jet_list[j]]);
        m_tree->Branch(m_jet_list[j] + "_seedEta", &m_jetSeed_eta[m_jet_list[j]]);
        m_tree->Branch(m_jet_list[j] + "_seedPhi", &m_jetSeed_phi[m_jet_list[j]]);
      }
    }
  }

  if(m_getEventInfo){
    m_tree->Branch("eventNumber", &eventNumber);
    m_tree->Branch("runNumber", &runNumber);
    m_tree->Branch("weight", &weight);
    //m_tree->Branch("mcEventWeight", &mcEventWeights);
    m_tree->Branch("distFrontBunchTrain", &distFrontBunchTrain);
    m_tree->Branch("BCID",&bcid);
    m_tree->Branch("averageInteractionsPerCrossing", &mu);
  }

  if(m_getCellsInfo){
    m_tree->Branch("cells_e", &cells_e);
    m_tree->Branch("cells_eta", &cells_eta);
    m_tree->Branch("cells_phi", &cells_phi);
    //m_tree->Branch("cells_et", &cells_et);
    m_tree->Branch("cells_time", &cells_time);
    m_tree->Branch("cells_quality", &cells_quality);
    m_tree->Branch("cells_provenance", &cells_provenance);
    //m_tree->Branch("cells_totalNoise", &cells_totalNoise);
  }

  m_recoTau_pt = {0.};
  m_recoTau_ptFinalCalib = {0.};
  m_recoTau_eta = {0.};
  m_recoTau_phi = {0.};
  m_recoTau_m = {0.};
  m_recoTau_nTaus = {0.};
  nTracksCharged = {0.};
  nTracksIsolation = {0.};
  nAllTracks = {0.};
  m_tree->Branch("recoTau_pt", &m_recoTau_pt);
  m_tree->Branch("recoTau_ptFinalCalib", &m_recoTau_ptFinalCalib);
  m_tree->Branch("recoTau_eta", &m_recoTau_eta);
  m_tree->Branch("recoTau_phi", &m_recoTau_phi);
  m_tree->Branch("recoTau_m", &m_recoTau_m);
  m_tree->Branch("recoTau_nTaus", &m_recoTau_nTaus);
  m_tree->Branch("nTracksCharged", &nTracksCharged);
  m_tree->Branch("nTracksIsolation", &nTracksIsolation);
  m_tree->Branch("nAllTracks", &nAllTracks);

  // Truth Taus
  m_truthTau_pt = {0.}; 
  m_truthTau_neutrinopt = {0.};
  m_truthTau_neutrinoeta = {0.};
  m_truthTau_neutrinophi = {0.};
  m_truthTau_totalpt = {0.};
  m_truthTau_eta = {0.}; 
  m_truthTau_phi = {0.}; 
  m_truthTau_nProng = {0}; 
  m_truthTau_nChargedMeson = {0}; 
  m_truthTau_nNeutralMeson = {0}; 
  m_truthTau_isHadronic = {false}; 
  m_truthTau_isLeptonicEle = {false}; 
  m_truthTau_outgoingTau = {false}; 
  m_tree->Branch("truthTau_pt", &m_truthTau_pt); 
  m_tree->Branch("truthTau_neutrinopt", &m_truthTau_neutrinopt);
  m_tree->Branch("truthTau_neutrinoeta", &m_truthTau_neutrinoeta);
  m_tree->Branch("truthTau_neutrinophi", &m_truthTau_neutrinophi);
  m_tree->Branch("truthTau_totalpt", &m_truthTau_totalpt);
  m_tree->Branch("truthTau_eta", &m_truthTau_eta); 
  m_tree->Branch("truthTau_phi", &m_truthTau_phi); 
  m_tree->Branch("truthTau_nProng", &m_truthTau_nProng); 
  m_tree->Branch("truthTau_nChargedMeson", &m_truthTau_nChargedMeson); 
  m_tree->Branch("truthTau_nNeutralMeson", &m_truthTau_nNeutralMeson); 
  m_tree->Branch("truthTau_isHadronic", &m_truthTau_isHadronic); 
  m_tree->Branch("truthTau_isLeptonicEle", &m_truthTau_isLeptonicEle); 
  m_tree->Branch("truthTau_outgoingTau", &m_truthTau_outgoingTau); 
  for (const auto &cl_name : m_cl_list){
    m_truthTau_nMatchedCluster[cl_name] = {0}; 
    m_truthTau_matchedClusters_et[cl_name] = {(std::vector<float>){0.}}; 
    m_truthTau_matchedClusters_dR[cl_name] = {(std::vector<float>){0.}}; 
    m_truthTau_matchedClusters_EngFracEM[cl_name] = {(std::vector<float>){0.}}; 
    m_tree->Branch("truthTau_"+cl_name+"_nMatchedCluster", &m_truthTau_nMatchedCluster[cl_name]); 
    m_tree->Branch("truthTau_"+cl_name+"_matchedClusters_et", &m_truthTau_matchedClusters_et[cl_name]); 
    m_tree->Branch("truthTau_"+cl_name+"_matchedClusters_dR", &m_truthTau_matchedClusters_dR[cl_name]); 
    m_tree->Branch("truthTau_"+cl_name+"_matchedClusters_EngFracEM", &m_truthTau_matchedClusters_EngFracEM[cl_name]); 
  }


  CHECK( histSvc()->regTree("/STREAM_TREE/ntuple",m_tree) );
  return StatusCode::SUCCESS;
}


StatusCode WriteOutputTree::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //
  return StatusCode::SUCCESS;
}


StatusCode WriteOutputTree::execute() {
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

  if(m_getEventInfo){
    CHECK( RetrieveEventInfo() );
  }
  /*
  if(m_getCellsInfo){
    CHECK( RetrieveCells() );
  }
  */
  if(!m_jet_list.empty()){
    CHECK( RetrieveJets() );
  }
  if(!m_cl_list.empty()){
    CHECK( RetrieveClusters() );
  }
  
  CHECK( RetrieveRecoTaus() );
  CHECK( RetrieveTruthTaus() );
  CHECK( RetrieveTruthMETs() ); 
  //-------

  m_tree->Fill();

  setFilterPassed(true);
  return StatusCode::SUCCESS;
}



//
StatusCode WriteOutputTree::RetrieveRecoTaus() {
    const xAOD::TauJetContainer* tau_jets = nullptr;
    CHECK(evtStore()->retrieve(tau_jets, "TauJets"));


    float nTaus = 0;
    m_recoTau_pt.clear();
    m_recoTau_ptFinalCalib.clear();
    m_recoTau_phi.clear();
    m_recoTau_eta.clear();
    m_recoTau_m.clear();
    m_recoTau_nTaus.clear();
    nTracksIsolation.clear();
    nTracksCharged.clear();
    nAllTracks.clear();

    for(auto tau : *tau_jets) {
        if(tau->pt() >= 30) {
	for(auto tau2 : *tau_jets) {
            if(tau2->pt() >= 30) {
	    if(abs(tau2->eta()) < 2.5 && abs(tau->eta()) < 2.5) {
		        if(abs(M_PI - tau->phi() - (M_PI - tau2->phi())) > 1.57) {
		           float pt = tau->pt();
        	    float phi = tau->phi();
        	    float eta = tau->eta();
        	    float m = tau->m();

        	    m_recoTau_pt.push_back(pt);
        	    m_recoTau_ptFinalCalib.push_back(tau->ptFinalCalib());
        	    m_recoTau_phi.push_back(phi);
        	    m_recoTau_eta.push_back(eta);
        	    m_recoTau_m.push_back(m);
              nTracksIsolation.push_back(tau->nTracksIsolation());
              nTracksCharged.push_back(tau->nTracksCharged());
              nAllTracks.push_back(tau->nAllTracks());

        	    nTaus++;
		    break;
		}
	    }
	    }
	}
	}
    }

    m_recoTau_nTaus.push_back(nTaus);


    return StatusCode::SUCCESS;
}

StatusCode WriteOutputTree::RetrieveEventInfo() {

  if(m_getEventInfo){
    mcEventWeights.clear();

    const xAOD::EventInfo* eventInfo = 0;
    CHECK( evtStore()->retrieve( eventInfo, "EventInfo" ) );

    mu = eventInfo->averageInteractionsPerCrossing();
    eventNumber = eventInfo->eventNumber();
    runNumber = eventInfo->runNumber();
    ToolHandle<Trig::IBunchCrossingTool> m_bcTool("Trig::MCBunchCrossingTool/BunchCrossingTool");
    distFrontBunchTrain = m_bcTool->distanceFromFront(eventInfo->bcid(), Trig::IBunchCrossingTool::BunchCrossings);
    bcid = eventInfo->bcid();
    mcEventWeights = eventInfo->mcEventWeights();
    weight = eventInfo->mcEventWeights()[0];
  }

  return StatusCode::SUCCESS;
}



StatusCode WriteOutputTree::RetrieveCells() {

  if(m_getCellsInfo){
    cells_e.clear();
    cells_eta.clear();
    cells_phi.clear();
    //cells_et.clear();
    cells_time.clear();
    cells_quality.clear();
    cells_provenance.clear();
    //cells_totalNoise.clear();

    // Cells info that changes between events
    const CaloCellContainer* cells = 0;
    CHECK( evtStore()->retrieve( cells, "AllCalo") );

    for(auto cell : *cells) {
      float e          = cell->energy();
      float eta        = cell->eta();
      float phi        = cell->phi();
      //float et         = cell->energy() * 1.0/TMath::CosH(cell->eta());
      float time       = cell->time();
      float quality    = cell->quality();
      float provenance = cell->provenance();
      //float totalNoise = m_noiseTool->getNoise(cell,ICalorimeterNoiseTool::TOTALNOISE);

      cells_e.push_back( e );
      cells_eta.push_back( eta );
      cells_phi.push_back( phi );
      //cells_et.push_back( et );
      cells_time.push_back( time );
      cells_quality.push_back( quality );
      cells_provenance.push_back( provenance );
      //cells_totalNoise.push_back( totalNoise );
    }
  }

  return StatusCode::SUCCESS;
}


//


StatusCode WriteOutputTree::RetrieveClusters() {

  for(const auto& cl_name: m_cl_list){
    m_cl_et[cl_name].clear();
    m_cl_eta[cl_name].clear();
    m_cl_phi[cl_name].clear();
    m_cl_EngFracEM[cl_name].clear();
    m_cl_m[cl_name].clear();
    m_cl_Ncells[cl_name].clear();

    const xAOD::CaloClusterContainer* clusters = nullptr;
    CHECK(evtStore()->retrieve(clusters,cl_name.Data()));
    ATH_MSG_DEBUG("Retrieved "+cl_name);

    for(auto iClust: *clusters){
      m_cl_et[cl_name].push_back( iClust->et()  );
      m_cl_eta[cl_name].push_back( iClust->eta() );
      m_cl_phi[cl_name].push_back( iClust->phi() );
      m_cl_EngFracEM[cl_name].push_back( iClust->getMomentValue(xAOD::CaloCluster_v1::MomentType(701)) );
      m_cl_m[cl_name].push_back( iClust->m() );

      // Ncells
      // custom clusters don't have cells info
      if(cl_name.Contains("Calo")){
	CaloClusterCellLink::const_iterator cellBegin = iClust->cell_begin();
	CaloClusterCellLink::const_iterator cellEnd = iClust->cell_end();
	int nCells = std::distance(cellBegin,cellEnd);
	m_cl_Ncells[cl_name].push_back( nCells );
      }
    }
  }

  return StatusCode::SUCCESS;
}


//


StatusCode WriteOutputTree::RetrieveJets() {

  for (const auto &jet_name : m_jet_list)
  {
    m_jet_pt[jet_name].clear();
    m_jet_eta[jet_name].clear();
    m_jet_phi[jet_name].clear();
    m_jet_m[jet_name].clear();
    m_jet_nConstituents[jet_name].clear();

    if (m_getJetConstituentsInfo)
    {
      m_jetConst_et[jet_name].clear();
      m_jetConst_eta[jet_name].clear();
      m_jetConst_phi[jet_name].clear();
    }

    if (m_getJetSeedsInfo)
    {
      m_jetSeed_et[jet_name].clear();
      m_jetSeed_eta[jet_name].clear();
      m_jetSeed_phi[jet_name].clear();
    }

      const xAOD::JetContainer *jets = nullptr;
      CHECK(evtStore()->retrieve(jets, jet_name.Data()));
      ATH_MSG_DEBUG("Retrieved xAOD " + jet_name);

      for (auto iJet : *jets)
      {
        m_jet_pt[jet_name].push_back(iJet->pt());
        m_jet_eta[jet_name].push_back(iJet->eta());
        m_jet_phi[jet_name].push_back(iJet->phi());
        m_jet_m[jet_name].push_back(iJet->m());
        m_jet_nConstituents[jet_name].push_back(iJet->numConstituents());

        // Jet's constituents information
        // if this information was not provided in TrigL0GepPerf, the constituent vector will be empty
        if (m_getJetConstituentsInfo)
        {
          std::vector<float> constVec_et;
          std::vector<float> constVec_eta;
          std::vector<float> constVec_phi;

          const xAOD::JetConstituentVector constvec = iJet->getConstituents();
          for (xAOD::JetConstituentVector::iterator it = constvec.begin(); it != constvec.end(); it++)
          {
            const xAOD::CaloCluster *cl = static_cast<const xAOD::CaloCluster *>((*it)->rawConstituent());
            constVec_et.push_back(cl->et());
            constVec_eta.push_back(cl->eta());
            constVec_phi.push_back(cl->phi());
          }
          m_jetConst_et[jet_name].push_back(constVec_et);
          m_jetConst_eta[jet_name].push_back(constVec_eta);
          m_jetConst_phi[jet_name].push_back(constVec_phi);
        }

        // Jet algorithm seeds information
        // if this information was not provided in TrigL0GepPerf or does not apply, the attributes are set to 0
        if (jet_name.Contains("Cone"))
        {
          if (m_getJetSeedsInfo)
          {
            m_jetSeed_et[jet_name].push_back(iJet->getAttribute<float>("SeedEt"));
            m_jetSeed_eta[jet_name].push_back(iJet->getAttribute<float>("SeedEta"));
            m_jetSeed_phi[jet_name].push_back(iJet->getAttribute<float>("SeedPhi"));
          }
        }
      }
    }

  return StatusCode::SUCCESS;
}

StatusCode WriteOutputTree::RetrieveTruthTaus() {
  m_truthTau_pt.clear(); 
  m_truthTau_neutrinopt.clear();
  m_truthTau_neutrinoeta.clear();
  m_truthTau_neutrinophi.clear();
  m_truthTau_totalpt.clear();
  m_truthTau_eta.clear(); 
  m_truthTau_phi.clear(); 
  m_truthTau_nProng.clear(); 
  m_truthTau_nChargedMeson.clear(); 
  m_truthTau_nNeutralMeson.clear(); 
  m_truthTau_isHadronic.clear(); 
  m_truthTau_isLeptonicEle.clear(); 
  m_truthTau_outgoingTau.clear(); 
  for (const auto &cl_name : ClustList){
    m_truthTau_nMatchedCluster[cl_name].clear(); 
    m_truthTau_matchedClusters_et[cl_name].clear(); 
    m_truthTau_matchedClusters_dR[cl_name].clear(); 
    m_truthTau_matchedClusters_EngFracEM[cl_name].clear(); 
  }

  const xAOD::CaloClusterContainer *clusters = nullptr;
  const xAOD::TruthParticleContainer *truth_particles = nullptr;
  CHECK (evtStore()->retrieve(truth_particles, "TruthParticles"));
  for (auto truth_particle : *truth_particles) {
    if (abs(truth_particle->pdgId()) != 15) continue;
    if (!(truth_particle->hasDecayVtx())) continue;
    TruthTau tau(truth_particle); 
    tau.process_decays(truth_particle); 
    for (const auto &cl_name : ClustList){
      CHECK(evtStore()->retrieve(clusters, cl_name));
      tau.set_matched_topoclusters(cl_name, clusters, .3, 1000); 
      m_truthTau_nMatchedCluster[cl_name].push_back(tau.n_matchedCluster[cl_name]); 
      m_truthTau_matchedClusters_et[cl_name].push_back(tau.matchedClusters_et[cl_name]); 
      m_truthTau_matchedClusters_dR[cl_name].push_back(tau.matchedClusters_dR[cl_name]); 
      m_truthTau_matchedClusters_EngFracEM[cl_name].push_back(tau.matchedClusters_EngFracEM[cl_name]); 
    }
    m_truthTau_pt.push_back(tau.pt); 
    m_truthTau_neutrinopt.push_back(tau.tauNeutrino_p4.Pt());
    m_truthTau_neutrinoeta.push_back(tau.tauNeutrino_p4.Eta());
    m_truthTau_neutrinophi.push_back(tau.tauNeutrino_p4.Phi());
    m_truthTau_totalpt.push_back(tau.total_p4.Pt());
    m_truthTau_eta.push_back(tau.eta); 
    m_truthTau_phi.push_back(tau.phi); 
    m_truthTau_nProng.push_back(tau.n_track); 
    m_truthTau_nChargedMeson.push_back(tau.n_charged_meson); 
    m_truthTau_nNeutralMeson.push_back(tau.n_neutral_meson); 
    m_truthTau_isHadronic.push_back(tau.is_hadronic); 
    m_truthTau_isLeptonicEle.push_back(tau.is_leptonic_ele); 
    m_truthTau_outgoingTau.push_back(tau.outgoing_tau); 
  }
  return StatusCode::SUCCESS; 
}

StatusCode WriteOutputTree::RetrieveTruthMETs() {
  m_truthMET_met.clear(); 
  m_truthMET_phi.clear(); 

  const xAOD::MissingETContainer *truthMETs = nullptr;
  CHECK (evtStore()->retrieve(truthMETs, "MET_Truth"));
  for (auto truthMET : *truthMETs) {
    m_truthMET_met.push_back(truthMET->met()); 
    m_truthMET_phi.push_back(truthMET->phi()); 
  }
  return StatusCode::SUCCESS; 
}
